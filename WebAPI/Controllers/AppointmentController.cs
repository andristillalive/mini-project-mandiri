﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ApointmentService.RequestModel;
using AppointmentService.Context;
using ApointmentService.Interface;
using Microsoft.AspNetCore.Authorization;
using AppointmentService.RequestModel;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentRepository _appointmentRepository;

        public AppointmentController(IAppointmentRepository appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
        }

        [HttpPost("Login")]
        public bool Login(string username, string password)
        {
            return _appointmentRepository.InterfaceLogin(username, password);
        }

        [HttpPost("InsertLogLogin")]
        public object InsertLogLogin(LogLoginModel loglogin)
        {
            return _appointmentRepository.InterfaceInsertLogLogin(loglogin);
        }

        [HttpPut("UpdateLogLogin")]
        public object UpdateLogLogin(LogLoginModel loglogin)
        {
            return _appointmentRepository.InterfaceUpdateLogLogin(loglogin);
        }

        [HttpGet("AppointmentReport")]
        public object AppointmentReport(DateTime date)
        {
            return _appointmentRepository.InterfaceAppointmentReport(date);
        }

        [HttpPost("InsertHoliday")]
        public object InsertHoliday(HolidayModel holiday)
        {
            return _appointmentRepository.InterfaceInsertHoliday(holiday);
        }

        [HttpPut("UpdateHoliday")]
        public object UpdateHoliday(HolidayModel holiday)
        {
            return _appointmentRepository.InterfaceUpdateHoliday(holiday);
        }

        [HttpDelete("DeleteHoliday")]
        public object DeleteHoliday(long holiday_id)
        {
            return _appointmentRepository.InterfaceDeleteHoliday(holiday_id);
        }

        [HttpPost("InsertMaxAppointment")]
        public object InsertMaxAppointment(MaxAppointmentModel maxappointment)
        {
            return _appointmentRepository.InterfaceInsertMaxAppointment(maxappointment);
        }

        [HttpPut("UpdateMaxAppointment")]
        public object UpdateMaxAppointment(MaxAppointmentModel maxappointment)
        {
            return _appointmentRepository.InterfaceUpdateMaxAppointment(maxappointment);
        }

        [HttpDelete("DeleteMaxAppointment")]
        public object DeleteMaxAppointment(long maxappointment_id)
        {
            return _appointmentRepository.InterfaceDeleteMaxAppointment(maxappointment_id);
        }

        [HttpPost("InsertCustomers")]
        public object InsertCustomers(CustomersModel customer)
        {
            return _appointmentRepository.InterfaceInsertCustomers(customer);
        }

        [HttpPut("UpdateCustomers")]
        public object UpdateCustomers(CustomersModel customer)
        {
            return _appointmentRepository.InterfaceUpdateCustomers(customer);
        }

        [HttpPost("InsertLogin")]
        public object InsertLogin(LoginModel login)
        {
            return _appointmentRepository.InterfaceInsertLogin(login);
        }

        [HttpPut("UpdateLogin")]
        public object UpdateLogin(LoginModel login)
        {
            return _appointmentRepository.InterfaceUpdateLogin(login);
        }

        [HttpPost("InsertAppointment")]
        public object InsertAppointment(AppointmentModel appointment)
        {
            return _appointmentRepository.InterfaceInsertAppointment(appointment);
        }
    }
}
