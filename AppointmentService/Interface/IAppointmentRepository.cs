﻿using ApointmentService.RequestModel;
using AppointmentService.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApointmentService.Interface
{
    public interface IAppointmentRepository
    {
        object InterfaceAppointmentReport(DateTime date);
        object InterfaceInsertHoliday(HolidayModel holidaymodel);
        object InterfaceDeleteHoliday(long holiday_id);
        object InterfaceUpdateHoliday(HolidayModel holidaymodel);
        object InterfaceInsertMaxAppointment(MaxAppointmentModel maxappointmentmodel);
        object InterfaceUpdateMaxAppointment(MaxAppointmentModel maxappointmentmodel);
        object InterfaceDeleteMaxAppointment(long maxappointment_id);
        object InterfaceInsertAppointment(AppointmentModel appointmentmodel);
        object InterfaceInsertCustomers(CustomersModel customersmodel);
        object InterfaceUpdateCustomers(CustomersModel customersmodel);
        bool InterfaceLogin(string username, string password);
        object InterfaceInsertLogin(LoginModel loginmodel);
        object InterfaceUpdateLogin(LoginModel loginmodel);
        object InterfaceInsertLogLogin(LogLoginModel logloginmodel);
        object InterfaceUpdateLogLogin(LogLoginModel logloginmodel);
    }
}
