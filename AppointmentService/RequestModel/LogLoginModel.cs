﻿namespace AppointmentService.RequestModel
{
    public class LogLoginModel
    {
        public long LogLoginId { get; set; }
        public long LoginId { get; set; }
    }
}
