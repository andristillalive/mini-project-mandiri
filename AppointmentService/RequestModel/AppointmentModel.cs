﻿namespace ApointmentService.RequestModel
{
    public class AppointmentModel
    {
        public long customer_id { get; set; }
        public string customer_name { get; set; }
        public DateTime appointment_date { get; set; }
    }
}
