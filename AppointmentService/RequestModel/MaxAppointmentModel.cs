﻿namespace ApointmentService.RequestModel
{
    public class MaxAppointmentModel
    {
        public long AppointmentMaxId { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public int max_number { get; set; }
        public bool isActive { get; set; }
    }
}
