﻿namespace AppointmentService.RequestModel
{
    public class LoginModel
    {
        public long LoginId { get; set; }
        public long CustomerId { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
