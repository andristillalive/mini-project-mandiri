﻿namespace ApointmentService.RequestModel
{
    public class HolidayModel
    {
        public long HolidayId { get; set; }
        public string? holiday_name { get; set; }
        public DateTime holiday_date { get; set; }
        public bool isActive { get; set; }
    }
}
