﻿namespace AppointmentService.RequestModel
{
    public class CustomersModel
    {
        public long CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? Address { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
