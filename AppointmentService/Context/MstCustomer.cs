﻿using System;
using System.Collections.Generic;

namespace AppointmentService.Context;

public partial class MstCustomer
{
    public long CustomerId { get; set; }

    public string? CustomerName { get; set; }

    public string? Address { get; set; }

    public bool? IsActive { get; set; }

    public bool? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string? UpdatedBy { get; set; }

    public virtual ICollection<MstLogin> MstLogins { get; set; } = new List<MstLogin>();

    public virtual ICollection<TrxAppointment> TrxAppointments { get; set; } = new List<TrxAppointment>();
}
