﻿using System;
using System.Collections.Generic;

namespace AppointmentService.Context;

public partial class TrxLogLogin
{
    public long LogLoginId { get; set; }

    public long? LoginId { get; set; }

    public DateTime? LoginDate { get; set; }

    public DateTime? LogoutDate { get; set; }

    public DateTime? CreatedDate { get; set; }

    public virtual MstLogin? Login { get; set; }
}
