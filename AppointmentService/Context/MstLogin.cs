﻿using System;
using System.Collections.Generic;

namespace AppointmentService.Context;

public partial class MstLogin
{
    public long LoginId { get; set; }

    public long? CustomerId { get; set; }

    public string? Username { get; set; }

    public string? Password { get; set; }

    public bool? IsActive { get; set; }

    public bool? IsDeleted { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public string? UpdatedBy { get; set; }

    public virtual MstCustomer? Customer { get; set; }

    public virtual ICollection<TrxLogLogin> TrxLogLogins { get; set; } = new List<TrxLogLogin>();
}
