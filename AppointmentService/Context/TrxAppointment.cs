﻿using System;
using System.Collections.Generic;

namespace AppointmentService.Context;

public partial class TrxAppointment
{
    public long AppointmentId { get; set; }

    public long? CustomerId { get; set; }

    public DateTime? AppointmentDate { get; set; }

    public DateTime? CreatedDate { get; set; }

    public string? CreatedBy { get; set; }

    public virtual MstCustomer? Customer { get; set; }
}
