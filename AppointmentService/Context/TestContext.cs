﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AppointmentService.Context;

public partial class TestContext : DbContext
{
    public TestContext()
    {
    }

    public TestContext(DbContextOptions<TestContext> options)
        : base(options)
    {
    }

    public virtual DbSet<MstCustomer> MstCustomers { get; set; }

    public virtual DbSet<MstHoliday> MstHolidays { get; set; }

    public virtual DbSet<MstLogin> MstLogins { get; set; }

    public virtual DbSet<MstMaxAppointment> MstMaxAppointments { get; set; }

    public virtual DbSet<TrxAppointment> TrxAppointments { get; set; }

    public virtual DbSet<TrxLogLogin> TrxLogLogins { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=DESKTOP-AVE6K0I;Database=TEST;Trusted_Connection=SSPI;Encrypt=false;TrustServerCertificate=true");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MstCustomer>(entity =>
        {
            entity.HasKey(e => e.CustomerId);

            entity.HasIndex(e => e.CustomerId, "NonClusteredIndex-20240609-170911");

            entity.Property(e => e.Address)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.CreatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.CustomerName)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
        });

        modelBuilder.Entity<MstHoliday>(entity =>
        {
            entity.HasKey(e => e.HolidayId);

            entity.Property(e => e.CreatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.HolidayDate).HasColumnType("datetime");
            entity.Property(e => e.HolidayName)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
        });

        modelBuilder.Entity<MstLogin>(entity =>
        {
            entity.HasKey(e => e.LoginId);

            entity.ToTable("MstLogin");

            entity.HasIndex(e => new { e.LoginId, e.CustomerId }, "NonClusteredIndex-20240609-170934");

            entity.Property(e => e.CreatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.Password)
                .HasMaxLength(250)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            entity.Property(e => e.Username)
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.HasOne(d => d.Customer).WithMany(p => p.MstLogins)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("LoginId_CustomerId_FK");
        });

        modelBuilder.Entity<MstMaxAppointment>(entity =>
        {
            entity.HasKey(e => e.AppointmentMaxId);

            entity.ToTable("MstMaxAppointment");

            entity.Property(e => e.CreatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.StartDate).HasColumnType("datetime");
            entity.Property(e => e.UpdatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
        });

        modelBuilder.Entity<TrxAppointment>(entity =>
        {
            entity.HasKey(e => e.AppointmentId);

            entity.ToTable("TrxAppointment");

            entity.HasIndex(e => e.CustomerId, "NonClusteredIndex-20240609-170959");

            entity.Property(e => e.AppointmentDate).HasColumnType("datetime");
            entity.Property(e => e.CreatedBy)
                .HasMaxLength(150)
                .IsUnicode(false);
            entity.Property(e => e.CreatedDate).HasColumnType("datetime");

            entity.HasOne(d => d.Customer).WithMany(p => p.TrxAppointments)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("TrxAppointment_CustomerId_FK");
        });

        modelBuilder.Entity<TrxLogLogin>(entity =>
        {
            entity.HasKey(e => e.LogLoginId);

            entity.ToTable("TrxLogLogin");

            entity.HasIndex(e => e.LoginId, "NonClusteredIndex-20240609-171017");

            entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            entity.Property(e => e.LoginDate).HasColumnType("datetime");
            entity.Property(e => e.LogoutDate).HasColumnType("datetime");

            entity.HasOne(d => d.Login).WithMany(p => p.TrxLogLogins)
                .HasForeignKey(d => d.LoginId)
                .HasConstraintName("TrxLogLogin_LoginId_FK");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
