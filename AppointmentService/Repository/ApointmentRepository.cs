﻿using AppointmentService.Context;
using ApointmentService.Interface;
using ApointmentService.RequestModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AppointmentService.RequestModel;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Globalization;

namespace ApointmentService.Repository
{
    public class ApointmentRepository : IAppointmentRepository
    {

        private readonly TestContext _context;
        public ApointmentRepository()
        {
            _context = new TestContext();
        }
        public object InterfaceAppointmentReport(DateTime date)
        {
            if (date == null || date.ToShortDateString() == "01/01/0001")
            {
                var result = _context.TrxAppointments.OrderBy(p => p.AppointmentDate).OrderByDescending(p => p.CreatedDate);
                return result;
            }
            else
            {
                var result = getAppointmentByDate(date);
                return result;
            }
        }

        public object InterfaceInsertAppointment(AppointmentModel appointmentmodel)
        {
            TrxAppointment trxappointment = new TrxAppointment();
            int maxnumber = getMaxNumber(appointmentmodel.appointment_date);
            int maxappointment = getNumberAppointmentByDate(appointmentmodel.appointment_date);
            DateTime appointment_date = DateTime.Now;
            if (maxappointment >= maxnumber)
            {
                appointment_date = appointmentmodel.appointment_date.AddDays(1);
            }
            else
            {
                appointment_date = appointmentmodel.appointment_date;
            }
            if (isHoliday(appointmentmodel.appointment_date) == false)
            {
                trxappointment.CustomerId = appointmentmodel.customer_id;
                trxappointment.AppointmentDate = appointment_date;//appointmentmodel.appointment_date;
                trxappointment.CreatedDate = DateTime.Now;
                _context.Add(trxappointment);
                _context.SaveChanges();
                return trxappointment;
            }
            else
            {
                return HttpStatusCode.BadRequest;
            }
        }

        public object InterfaceInsertHoliday(HolidayModel holidaymodel)
        {
            MstHoliday mstholiday = new MstHoliday();
            mstholiday.HolidayName = holidaymodel.holiday_name;
            mstholiday.HolidayDate = holidaymodel.holiday_date;
            mstholiday.IsActive = true;
            mstholiday.IsDeleted = false;
            mstholiday.CreatedDate = DateTime.Now;
            _context.Add(mstholiday);
            _context.SaveChanges();
            return mstholiday;
        }
        public object InterfaceUpdateHoliday(HolidayModel holidaymodel)
        {
            var dataHoliday = from p in _context.MstHolidays
                              where p.HolidayId == holidaymodel.HolidayId
                              select p;
            MstHoliday mstholiday = new MstHoliday();
            if (dataHoliday.Any())
            {
                mstholiday = dataHoliday.FirstOrDefault();
                mstholiday.HolidayName = holidaymodel.holiday_name;
                mstholiday.HolidayDate = holidaymodel.holiday_date;
                mstholiday.IsActive = holidaymodel.isActive;
                mstholiday.UpdatedDate = DateTime.Now;
                _context.Update(mstholiday);
                _context.SaveChanges();
            }
            return mstholiday;
        }
        public object InterfaceDeleteHoliday(long holiday_id)
        {
            var dataHoliday = from p in _context.MstHolidays
                              where p.HolidayId == holiday_id
                              select p;
            MstHoliday mstholiday = new MstHoliday();
            if (dataHoliday.Any())
            {
                mstholiday = dataHoliday.FirstOrDefault();
                _context.Remove(mstholiday);
                _context.SaveChanges();
            }
            return mstholiday;
        }

        public object InterfaceInsertMaxAppointment(MaxAppointmentModel maxappointmentmodel)
        {
            MstMaxAppointment mstmaxappointment = new MstMaxAppointment();
            mstmaxappointment.StartDate = maxappointmentmodel.start_date;
            mstmaxappointment.EndDate = maxappointmentmodel.end_date;
            mstmaxappointment.MaxNumber = maxappointmentmodel.max_number;
            mstmaxappointment.IsActive = true;
            mstmaxappointment.IsDeleted = false;
            mstmaxappointment.CreatedDate = DateTime.Now;
            _context.Add(mstmaxappointment);
            _context.SaveChanges();
            return mstmaxappointment;
        }
        public object InterfaceUpdateMaxAppointment(MaxAppointmentModel maxappointmentmodel)
        {
            var dataMaxApp = from p in _context.MstMaxAppointments
                             where p.AppointmentMaxId == maxappointmentmodel.AppointmentMaxId
                             select p;
            MstMaxAppointment mstmaxappointment = new MstMaxAppointment();
            if (dataMaxApp.Any())
            {
                mstmaxappointment = dataMaxApp.FirstOrDefault();
                mstmaxappointment.StartDate = maxappointmentmodel.start_date;
                mstmaxappointment.EndDate = maxappointmentmodel.end_date;
                mstmaxappointment.MaxNumber = maxappointmentmodel.max_number;
                mstmaxappointment.IsActive = maxappointmentmodel.isActive;
                mstmaxappointment.UpdatedDate = DateTime.Now;
                _context.Update(mstmaxappointment);
                _context.SaveChanges();
            }
            return mstmaxappointment;
        }
        public object InterfaceDeleteMaxAppointment(long maxappointment_id)
        {
            var dataMaxApp = from p in _context.MstMaxAppointments
                             where p.AppointmentMaxId == maxappointment_id
                             select p;
            MstMaxAppointment mstmaxappointment = new MstMaxAppointment();
            if (dataMaxApp.Any())
            {
                mstmaxappointment = dataMaxApp.FirstOrDefault();
                _context.Remove(mstmaxappointment);
                _context.SaveChanges();
            }
            return mstmaxappointment;
        }
        public object InterfaceInsertCustomers(CustomersModel customersmodel)
        {
            MstCustomer mstcust = new MstCustomer();
            mstcust.CustomerName = customersmodel.CustomerName;
            mstcust.Address = customersmodel.Address;
            mstcust.IsActive = true;
            mstcust.IsDeleted = false;
            mstcust.CreatedDate = DateTime.Now;
            _context.Add(mstcust);
            _context.SaveChanges();
            return mstcust;
        }
        public object InterfaceUpdateCustomers(CustomersModel customersmodel)
        {
            var dataCust = from p in _context.MstCustomers
                           where p.CustomerId == customersmodel.CustomerId
                           select p;
            MstCustomer mstcust = new MstCustomer();
            if (dataCust.Any())
            {
                mstcust = dataCust.FirstOrDefault();
                mstcust.CustomerName = customersmodel.CustomerName;
                mstcust.Address = customersmodel.Address;
                mstcust.IsActive = customersmodel.IsActive;
                mstcust.UpdatedDate = DateTime.Now;
                _context.Update(mstcust);
                _context.SaveChanges();
            }
            return mstcust;
        }
        public bool InterfaceLogin(string username, string password)
        {
            var dataLogin = from p in _context.MstLogins
                            where p.Username == username && p.Password == password
                            select p;

            if (dataLogin.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public object InterfaceInsertLogin(LoginModel loginmodel)
        {
            MstLogin mstlogin = new MstLogin();
            mstlogin.CustomerId = loginmodel.CustomerId;
            mstlogin.Username = loginmodel.Username;
            mstlogin.Password = loginmodel.Password;
            mstlogin.IsActive = true;
            mstlogin.IsDeleted = false;
            mstlogin.CreatedDate = DateTime.Now;
            _context.Add(mstlogin);
            _context.SaveChanges();
            return mstlogin;
        }
        public object InterfaceUpdateLogin(LoginModel loginmodel)
        {
            var datalogin = from p in _context.MstLogins
                            where p.LoginId == loginmodel.LoginId
                            select p;
            MstLogin mstlogin = new MstLogin();
            if (datalogin.Any())
            {
                mstlogin = datalogin.FirstOrDefault();
                mstlogin.Username = loginmodel.Username;
                mstlogin.Password = loginmodel.Password;
                mstlogin.UpdatedDate = DateTime.Now;
                _context.Update(mstlogin);
                _context.SaveChanges();
            }
            return mstlogin;
        }
        public object InterfaceInsertLogLogin(LogLoginModel logloginmodel)
        {
            TrxLogLogin mstloglogin = new TrxLogLogin();
            mstloglogin.LoginId = logloginmodel.LoginId;
            mstloglogin.LoginDate = DateTime.Now;
            mstloglogin.CreatedDate = DateTime.Now;
            _context.Add(mstloglogin);
            _context.SaveChanges();
            return mstloglogin;
        }
        public object InterfaceUpdateLogLogin(LogLoginModel logloginmodel)
        {
            var dataLogLogin = from p in _context.TrxLogLogins
                               where p.LogLoginId == logloginmodel.LogLoginId
                               select p;
            TrxLogLogin mstloglogin = new TrxLogLogin();
            if (dataLogLogin.Any())
            {

                mstloglogin = dataLogLogin.FirstOrDefault();
                mstloglogin.LogoutDate = DateTime.Now;
                _context.Update(mstloglogin);
                _context.SaveChanges();
            }
            return mstloglogin;
        }

        private bool isHoliday(DateTime date)
        {
            var mstHoliday = from p in _context.MstHolidays
                             where p.HolidayDate == date
                             select p;
            if (mstHoliday.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private int getMaxNumber(DateTime date)
        {
            var mstMaxNumber = _context.MstMaxAppointments.FirstOrDefault(p => p.StartDate <= date.Date && p.EndDate >= date.Date);
            if (mstMaxNumber == null)
            {
                return 5;
            }
            else
            {
                return Convert.ToInt32(mstMaxNumber.MaxNumber);
            }
        }
        private int getNumberAppointmentByDate(DateTime date)
        {
            var numberAppointment = from p in _context.TrxAppointments
                                    where p.AppointmentDate == date
                                    select p;
            if (numberAppointment == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(numberAppointment.Count());
            }
        }
        private object getAppointmentByDate(DateTime date)
        {
            var Appointment = from p in _context.TrxAppointments
                              where p.AppointmentDate == date
                              select p;
            return Appointment;
        }
    }
}
