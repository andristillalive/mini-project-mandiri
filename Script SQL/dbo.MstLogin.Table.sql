USE [TEST]
GO
/****** Object:  Table [dbo].[MstLogin]    Script Date: 09/06/2024 19:23:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstLogin](
	[LoginId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NULL,
	[Username] [varchar](150) NULL,
	[Password] [varchar](250) NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](150) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](150) NULL,
 CONSTRAINT [PK_MstLogin] PRIMARY KEY CLUSTERED 
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20240609-170934]    Script Date: 09/06/2024 19:23:03 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20240609-170934] ON [dbo].[MstLogin]
(
	[LoginId] ASC,
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MstLogin]  WITH CHECK ADD  CONSTRAINT [LoginId_CustomerId_FK] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[MstCustomers] ([CustomerId])
GO
ALTER TABLE [dbo].[MstLogin] CHECK CONSTRAINT [LoginId_CustomerId_FK]
GO
