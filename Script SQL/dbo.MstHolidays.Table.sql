USE [TEST]
GO
/****** Object:  Table [dbo].[MstHolidays]    Script Date: 09/06/2024 19:23:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MstHolidays](
	[HolidayId] [bigint] IDENTITY(1,1) NOT NULL,
	[HolidayName] [varchar](150) NULL,
	[HolidayDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](150) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](150) NULL,
 CONSTRAINT [PK_MstHolidays] PRIMARY KEY CLUSTERED 
(
	[HolidayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
