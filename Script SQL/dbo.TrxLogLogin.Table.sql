USE [TEST]
GO
/****** Object:  Table [dbo].[TrxLogLogin]    Script Date: 09/06/2024 19:23:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrxLogLogin](
	[LogLoginId] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginId] [bigint] NULL,
	[LoginDate] [datetime] NULL,
	[LogoutDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_TrxLogLogin] PRIMARY KEY CLUSTERED 
(
	[LogLoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20240609-171017]    Script Date: 09/06/2024 19:23:03 ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20240609-171017] ON [dbo].[TrxLogLogin]
(
	[LoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TrxLogLogin]  WITH CHECK ADD  CONSTRAINT [TrxLogLogin_LoginId_FK] FOREIGN KEY([LoginId])
REFERENCES [dbo].[MstLogin] ([LoginId])
GO
ALTER TABLE [dbo].[TrxLogLogin] CHECK CONSTRAINT [TrxLogLogin_LoginId_FK]
GO
